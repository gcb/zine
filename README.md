# zine

TODO:

* [X] <s>hugo?</s> Nope. feature creep. requires command line tools. can't be text editor only.
* [X] pelican?
* [X] gitlab ci
* [X] gitlab pages
* [X] custom domain
   * [X] decolonialzine.fashion/
   * [X] www.decolonialzine.fashion/
      * [ ] redirect to canonical
   * [X] decolonial.zinefashion.org/
      * [ ] redirect to canonical
   * [X] zinefashion.org/ (temporary. might become an index)
      * [ ] redirect to canonical
* [X] Template
* [X] Logo?
* [X] PDFs?
* [X] "posts"
* [X] CDN?



## Authors and acknowledgment
TODO;

## License
AGPLv3

## Project status
1.0
