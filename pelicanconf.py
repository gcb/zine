#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# https://docs.getpelican.com/en/latest/settings.html

AUTHOR = "MiMedrado"
SITEURL = "https://decolonialzine.fashion"
FEED_DOMAIN = SITEURL
SITENAME = "Decolonial zine on Fashion"
SITETITLE = "Decolonial zine on Fashion"
SITESUBTITLE = ""
SITEDESCRIPTION = "Decoloniality and Fashion e-Zine"
SITELOGO = SITEURL + "/i/profile.png"
FAVICON = SITEURL + "/i/favicon.ico"
TIMEZONE = "UTC"
DEFAULT_DATE_FORMAT = "%Y-%m-%d(%a)"
DEFAULT_LANG = "en"
DEFAULT_CATEGORY = "zine"

BROWSER_COLOR = "gold"
ROBOTS = "index, follow"

CC_LICENSE = {
    "name": "Creative Commons Attribution-ShareAlike",
    "version": "4.0",
    "slug": "by-sa"
}

COPYRIGHT_YEAR = 2023

OUTPUT_PATH = "public/"
PATH = "content/"
# static_path is inside PATH
STATIC_PATHS = ["i/"]
EXTRA_PATH_METADATA = {
    'i/robots.txt': {'path': 'robots.txt'}
}


# absolute path from root:
CUSTOM_CSS = "i/custom.css"

MAIN_MENU = False

#ADD_THIS_ID = "ra-640df3c92ff02f7d"
#DISQUS_SITENAME = "dfzine"
#GOOGLE_ANALYTICS = "UA-1234-5678"
#GOOGLE_TAG_MANAGER = "GTM-ABCDEF"
USE_GOOGLE_FONTS = False

PLUGINS = ["sitemap"]

# Enable i18n plugin.
#PLUGINS = ["i18n_subsites"]
#I18N_TEMPLATES_LANG = "en"
# Enable Jinja2 i18n extension used to parse translations.
#JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}

THEME = "themes-custom/Flex/"

# https://github.com/pelican-plugins/sitemap/issues/15
TEMPLATE_PAGES = {}

