
## 2.4.0-gcb1

- mostly tweak social sharing and unify some page types
- hack to fix shared url (canonize index.html as / etc...)

* 1b4f14a 2023-03-22 | move text around [gcb]
* 7bb206b 2023-03-22 | Revert "flair start" [gcb]
* 42669ef 2023-03-22 | update Abouts [gcb]
* c7c76a0 2023-03-22 | flair start [gcb]
* 96bc9bf 2023-03-15 | clean. use title instead of alt [gcb]
* 617bb72 2023-03-15 | move tips to img [gcb]
* 3469235 2023-03-15 | tooltips [gcb]
* 0d163ce 2023-03-15 | simplify css [gcb]
* aa11ad2 2023-03-15 | fix output_file usage [gcb]
* 49d154a 2023-03-15 | test output_file var [gcb]
* d3d8a6d 2023-03-12 | meh [gcb]
* d424b3f 2023-03-12 | content [gcb]
* baa3830 2023-03-12 | dock effect [gcb]
* 2b4316e 2023-03-12 | fix shared urls [gcb]
* 49ca208 2023-03-12 | hack to fix url [gcb]
* c356caa 2023-03-12 | share on all pages [gcb]
* 3a0ae16 2023-03-12 | fix fb [gcb]
* 876cddc 2023-03-12 | fix buttons [gcb]
* cc927ab 2023-03-12 | mo buttons [gcb]
* a4fa2ba 2023-03-12 | mo buttons [gcb]
* 48f6f54 2023-03-12 | WIP social buttons [gcb]
* 3b66939 2023-03-12 | less social social sharing [gcb]


## 2.4.0

- Update translations (Italian, Spanish, Portuguese, German, Turkish)
- Include settings for canonical URLS [#238](https://github.com/alexandrevicenzi/Flex/issues/238)
- Remove pelican plugins dependency [#255](https://github.com/alexandrevicenzi/Flex/issues/255)

This version includes cs, de, en, es, es_ES, et, fa_IR, fr, hu_HU, id, it, nl_NL, pl_PL, pt, pt_BR, pt_PT, ro_RO, ru, tr_TR, zh_CN translations.

## 2.3.0

- Support dark theme [#213](https://github.com/alexandrevicenzi/Flex/pull/213)
- Support Isso comments [#200](https://github.com/alexandrevicenzi/Flex/pull/200)
- Support Pelican 4.5 [#248](https://github.com/alexandrevicenzi/Flex/pull/248)
- Support Tipue search [#193](https://github.com/alexandrevicenzi/Flex/pull/193)
- Upgrade Font-Awesome to 5 [#156](https://github.com/alexandrevicenzi/Flex/pull/156)
- Add social icons (keybase, twitch, mastodon, diaspora, flickr, last.fm
- Fix layouts
- Update translations (zh)

This version includes cs, de, en, es, es_ES, et, fa_IR, fr, hu_HU, id, it, nl_NL, pl_PL, pt, pt_BR, pt_PT, ro_RO, ru, tr_TR, zh_CN translations.

## 2.2.0

- Update Pygments - New themes available!
- Fix code highlight [#125](https://github.com/alexandrevicenzi/Flex/pull/125)
- New social icons
- Remove duoshuo integration [#139](https://github.com/alexandrevicenzi/Flex/pull/139)
- Other small bug fixes

This version includes de, en, es, es_ES, et, fa_IR, fr, hu_HU, it, nl_NL, pl_PL, pt_BR, ru, tr_TR, zh_CN translations.

## 2.1.0

- Italian translation [#60](https://github.com/alexandrevicenzi/Flex/issues/60)
- Hungarian transltion [#59](https://github.com/alexandrevicenzi/Flex/issues/59)
- Russian transltion [#58](https://github.com/alexandrevicenzi/Flex/issues/58)
- [Google AdSense support](https://github.com/alexandrevicenzi/Flex/issues/47)

## 2.0.0

- [Minute read like Medium](https://github.com/alexandrevicenzi/Flex/issues/48) (via plugin)
- [Theme translations](https://github.com/alexandrevicenzi/Flex/wiki/Translation-support)
- Updated font-awesome
- Changed `Cover` metadata to use relative path

This version includes de, fr and pt_BR translations.

Special thanks to @marcelhuth.

## 1.2.0

- [Update font-awesome](https://github.com/alexandrevicenzi/Flex/issues/31)
- [Added browser color configuration](https://github.com/alexandrevicenzi/Flex/pull/34)
- [Related posts](https://github.com/alexandrevicenzi/Flex/pull/27)
- [More Pygments Styles](https://github.com/alexandrevicenzi/Flex/issues/38)
- [Add StatusCake RUM support](https://github.com/alexandrevicenzi/Flex/issues/16)

## 1.1.1

- [Bug in CSS with placement of "Newer Posts" button](https://github.com/alexandrevicenzi/Flex/issues/21)
- [Posts preview on main page](https://github.com/alexandrevicenzi/Flex/issues/14)
- [Strip HTML tags from title](https://github.com/alexandrevicenzi/Flex/pull/25)
- [Added style for reddit social link](https://github.com/alexandrevicenzi/Flex/pull/23)

## 1.1.0

- [Allow custom CSS stylesheets to override the default one](https://github.com/alexandrevicenzi/Flex/pull/9)
- [Add Windows-specific font variants](https://github.com/alexandrevicenzi/Flex/pull/8)
- [Move the "tagged" bullet inside the conditional](https://github.com/alexandrevicenzi/Flex/pull/7)
- [Add stack-overflow to supported social icons](https://github.com/alexandrevicenzi/Flex/pull/6)
- [Use THEME_STATIC_DIR for asset URL's](https://github.com/alexandrevicenzi/Flex/pull/5)
- [show summary for articles in index.html](https://github.com/alexandrevicenzi/Flex/pull/4)
- [Fixed email icon bug](https://github.com/alexandrevicenzi/Flex/pull/3)

## 1.0.0

First release.
